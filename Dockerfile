FROM python:3.10.6-alpine

WORKDIR /app

COPY pyPhasesRecordloader /app/pyPhasesRecordloader
COPY tests /app/tests
COPY requirements.txt /app

# Install dependencies
RUN apk update && apk add --no-cache build-base && \
    pip install --upgrade pip && \
    pip install -r requirements.txt

ENTRYPOINT [ "python", "-m", "unittest", "discover", "-s", "tests" ]

# Copy the current directory contents into the container at /app