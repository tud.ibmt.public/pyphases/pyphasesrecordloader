import unittest
from unittest.mock import patch
import numpy as np

from pyPhasesRecordloader.EventManager import EventManager, Event

class TestEventManager(unittest.TestCase):

    def setUp(self):
        self.event_groups = {
            'group1': ['event1', 'event2'],
            'group2': ['event3', 'event4']
        }
        self.event_manager = EventManager(self.event_groups)

    def test_get_events_from_signal(self):
        signal = [0, 1, 0, 2, 0]
        events = self.event_manager.getEventsFromSignal(signal, ['None', 'event1', 'event2'], ignore=0)
        
        self.assertEqual(len(events), 2)
        self.assertEqual(events[0].name, 'event1')
        self.assertEqual(events[0].start, 1)
        self.assertEqual(events[0].duration, 1)
        self.assertEqual(events[1].name, 'event2')
        self.assertEqual(events[1].start, 3)
        self.assertEqual(events[1].duration, 1)

    def test_get_events_from_signal_ordered(self):
        signal = [0, 1, 0, 0]
        events = self.event_manager.getEventsFromSignal(signal, ['None','event1'], ignore=1)
        
        self.assertEqual(events[0].name, 'None')
        self.assertEqual(events[0].start, 0)
        self.assertEqual(events[0].duration, 1)
        self.assertEqual(events[1].name, 'None')
        self.assertEqual(events[1].start, 2)
        self.assertEqual(events[1].duration, 2)

