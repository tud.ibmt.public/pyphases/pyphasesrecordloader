import unittest
from unittest.mock import patch, MagicMock

from pyPhasesRecordloader.RecordLoader import RecordLoader
from pyPhasesRecordloader.downloader.Downloader import Downloader

class RecordLoaderTest(unittest.TestCase):
    def setUp(self):
        # Create an instance of RecordLoader for testing
        self.recordLoader = RecordLoader()
        self.recordLoader.downloader = Downloader({
            "type": "allFromFolder",
            "basePath": ".",            
        })

    def test_setupRemoteReadOrDownload_canReadRemote(self):
        self.recordLoader.downloader.canReadRemote = True

        self.recordLoader.setupRemoteReadOrDownload()

        # Assert that the filePath is set to the base path of the downloader
        self.assertEqual(self.recordLoader.filePath, self.recordLoader.downloader.basePath)

    def test_setupRemoteReadOrDownload_filePathExists(self):
        # Mocking the exists method of Path to return True
        with patch('pathlib.Path.exists') as mock_exists:
            mock_exists.return_value = True

            self.recordLoader.setupRemoteReadOrDownload()

            # Assert that the filePath remains the same
            self.assertEqual(self.recordLoader.filePath, "")

    def test_setupRemoteReadOrDownload_filePathDoesNotExist(self):
        # Mocking the exists method of Path to return False
        with patch('pathlib.Path.exists') as mock_exists:
            mock_exists.return_value = False
            
            self.recordLoader.downloader.downloadTo = MagicMock()
            self.recordLoader.setupRemoteReadOrDownload()
            self.recordLoader.downloader.downloadTo.assert_called_once_with(self.recordLoader.filePath)