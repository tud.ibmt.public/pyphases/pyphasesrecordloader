from io import StringIO
import unittest


from pyPhasesRecordloader.recordLoaders.CSVMetaLoader import CSVMetaLoader


class TestCSVMetaLoader(unittest.TestCase):
    def setUp(self):
        # Create a sample CSV in-memory
        csv_data = """id,name,age,location
r1,Alice,30,New York
r2,Bob,40,San Francisco
r3,Charlie,50,London
"""
        self.csv_file_path = StringIO(csv_data)
        self.loader = CSVMetaLoader(self.csv_file_path, "id", {"name": "name", "myAge": "age"})
        # self.df = pd.read_csv(StringIO(csv_data))

    def test_init(self):
        self.assertIsNotNone(self.loader.df)
        self.assertEqual(self.loader.idColumn, "id")
        self.assertEqual(self.loader.relevantRows, {"name": "name", "myAge": "age"})

    def test_getMetaData_empty(self):
        meta = self.loader.getMetaData("r0")
        self.assertEqual(meta, {})

    def test_getMetaData(self):
        meta = self.loader.getMetaData("r2")
        self.assertEqual(meta, {"name": "Bob", "myAge": 40})
    
    def test_getMetaData_getRowId(self):
        
        self.loader.getRowId = lambda r: f"r{r}"
        meta = self.loader.getMetaData("1")
        self.assertEqual(meta, {"name": "Alice", "myAge": 30})
