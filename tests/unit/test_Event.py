import unittest
from pyPhasesRecordloader.Event import Event

class TestEvent(unittest.TestCase):

    def test_end(self):
        event = Event(start=5, duration=10)
        self.assertEqual(event.end(), 15)

    def test_update_frequency(self):
        event = Event(start=1, duration=2, frequency=10)
        event.updateFrequency(5)
        self.assertEqual(event.start, 0.5)
        self.assertEqual(event.duration, 1)
        self.assertEqual(event.frequency, 5)

    def test_update_frequency_with_original(self):
        event = Event(start=1, duration=2, frequency=10)
        event.updateFrequency(10, originalFrequency=5)
        self.assertEqual(event.start, 2)
        self.assertEqual(event.duration, 4)
        self.assertEqual(event.frequency, 10)

    def test_todict(self):
        event = Event(name='test', start=1.5, duration=2.5, amplitude=0.8)
        expected = {
            "name": "test",
            "start": 1.5,
            "duration": 2.5,
            "amplitude": 0.8,
            "power": 0,
            "frequency": 1,
            "owner": None,
            "manual": False,
            "data": {}
        }
        self.assertEqual(event.todict(), expected)

    def test_fromdict(self):
        d = {"name": "event1", "start": 5, "duration": 10}
        event = Event.fromdict(d)
        self.assertEqual(event.name, "event1")
        self.assertEqual(event.start, 5)
        self.assertEqual(event.duration, 10)
